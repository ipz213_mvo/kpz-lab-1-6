﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary
{
    public class Memento
    {
        public char[] GameBoardState { get; set; }
        public int PlayerState { get; set; }
        public int BoardPosition { get; set; }
    }
}
