﻿using Newtonsoft.Json;
using System.Diagnostics;
using System.Reflection.Metadata;

namespace GameLibrary
{
    public static class Game
    {
        private static char[] boardPositions = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        private static Player playerOne = new Player('X');
        private static Player playerTwo = new Player('O');
        private static string choise;
        static int player = 1;
        static int flag = 0;
        static bool isSinglePlayer = false;
        private static SimpleAI ai = new SimpleAI();
        private static List<Memento> history = new List<Memento>();


        public static void StartGame(int mode)
        {
            Console.WriteLine("Would you like to play single player mode? (y/n)");
            string gameModeResponse = Console.ReadLine();
            if (gameModeResponse.ToLower() == "y")
            {
                isSinglePlayer = true;
            }

            Console.WriteLine("Would you like to load a saved game? (y/n)");
            string loadGameResponse = Console.ReadLine();
            if (loadGameResponse.ToLower() == "y")
            {
                LoadGameFromFile("saved-game.json");
                Console.WriteLine("Game has been loaded.");
            }

            do
            {
                Board(mode);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.S)
                {
                    SaveGameToFile("saved-game.json");
                    Console.WriteLine("Game has been saved.");
                    continue;
                }
                if (keyInfo.Key == ConsoleKey.U)
                {
                    UndoMove();
                    Board(mode);
                    continue;
                }

                string choice;
                if (!isSinglePlayer || player % 2 != 0)
                {
                    choice = keyInfo.KeyChar.ToString();
                }
                else
                {
                    choice = ai.MakeMove(boardPositions).ToString();
                }

                if (int.TryParse(choice, out int playerChoice))
                {
                    if (playerChoice <= 9 && playerChoice != 0)
                    {
                        if (CheckPosition(playerChoice) != true)
                        {
                            if (player % 2 == 0)
                            {
                                boardPositions[playerChoice] = playerTwo.Name;
                                player++;
                                SaveToHistory(playerChoice);
                            }
                            else
                            {
                                boardPositions[playerChoice] = playerOne.Name;
                                player++;
                                SaveToHistory(playerChoice);
                            }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine($"Cell \"{playerChoice}\" is already set.");
                            Console.ResetColor();
                            System.Threading.Thread.Sleep(3000);
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"There is no cell \"{playerChoice}\" on the field.");
                        Console.ResetColor();
                        System.Threading.Thread.Sleep(3000);
                    }
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Please\r\nenter a valid number between 1 and 9.");
                    Console.ResetColor();
                    System.Threading.Thread.Sleep(3000);
                }
                flag = CheckWin();
            } while (flag != 1 && flag != -1);

            Board(mode);
            if (flag == -1)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("It’s a draw.");
                Console.ResetColor();
                ResetGame();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"Player {(player % 2) + 1} wins");
                Console.ResetColor();
                if (((player % 2) + 1) == 1)
                {
                    playerOne.AddWin();
                }
                else
                {
                    playerTwo.AddWin();
                }
                ResetGame();
            }
        }



        private static int CheckWin()
        {
            //Vertical
            if (boardPositions[1] == boardPositions[4] && boardPositions[4] == boardPositions[7])
            {
                return 1;
            }
            if(boardPositions[2] == boardPositions[5] && boardPositions[5] == boardPositions[8])
            {
                return 1;
            }
            if(boardPositions[3] == boardPositions[6] && boardPositions[6] == boardPositions[9])
            {
                return 1;
            }
            //Horizontal 
            if (boardPositions[1] == boardPositions[2] && boardPositions[2] == boardPositions[3])
            {
                return 1;
            }
            if (boardPositions[4] == boardPositions[5] && boardPositions[5] == boardPositions[6])
            {
                return 1;
            }
            if (boardPositions[3] == boardPositions[6] && boardPositions[6] == boardPositions[9])
            {
                return 1;
            }
            //Diagonal 
            if (boardPositions[1] == boardPositions[5] && boardPositions[5] == boardPositions[9])
            {
                return 1;
            }
            if (boardPositions[3] == boardPositions[5] && boardPositions[5] == boardPositions[7])
            {
                return 1;
            }
            //Draw
            if(boardPositions[1] != '1' && boardPositions[2] != '2' && boardPositions[3] != '3' && boardPositions[4] != '4' && boardPositions[5] != '5' && boardPositions[6] != '6' && boardPositions[7] != '7' && boardPositions[8] != '8' && boardPositions[9] != '9')
            {
                return -1;
            }
            return 0;
        }

        private static bool CheckPosition(int choise)
        {
            if (boardPositions[choise] == 'X' || boardPositions[choise] == 'O')
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void Board(int mode)
        {
            Console.Clear();
            Console.WriteLine("Let's play Tic Tac Toe!");
            if(mode == 0)
            {
                Console.WriteLine("Player 1: X\nPlayer 2: O");
            }
            else
            {
                Console.WriteLine($"\n\nPlayer 1: X [{playerOne.CountWin}]\nPlayer 2: O [{playerTwo.CountWin}]");
            }
            if (player % 2 == 0)
            {
                Console.WriteLine("\n\nPlayer 2's turn. Select from 1 to 9 from the board");
            }
            else
            {
                Console.WriteLine("\n\nPlayer 1's turn. Select from 1 to 9 from the board");
            }
            Console.WriteLine("\n");

            Console.WriteLine($" {boardPositions[1]} | {boardPositions[2]} | {boardPositions[3]}");
            Console.WriteLine("---+---+---");
            Console.WriteLine($" {boardPositions[4]} | {boardPositions[5]} | {boardPositions[6]}");
            Console.WriteLine("---+---+---");
            Console.WriteLine($" {boardPositions[7]} | {boardPositions[8]} | {boardPositions[9]}");
            Console.WriteLine("\n");
        }
        public static void ResetGame()
        {
            boardPositions = new char[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            player = 1;
        }
        public static Memento SaveState()
        {
            return new Memento
            {
                GameBoardState = (char[])boardPositions.Clone(),
                PlayerState = player
            };
        }

        public static void LoadState(Memento memento)
        {
            boardPositions = memento.GameBoardState;
            player = memento.PlayerState;
        }
        public static void SaveGameToFile(string fileName)
        {
            Memento memento = SaveState();
            string jsonData = JsonConvert.SerializeObject(memento);
            File.WriteAllText(fileName, jsonData);
        }

        public static void LoadGameFromFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                string jsonData = File.ReadAllText(fileName);
                Memento memento = JsonConvert.DeserializeObject<Memento>(jsonData);
                LoadState(memento);
            }
            else
            {
                Console.WriteLine("No saved game was found!");
            }
        }
        public static void SaveToHistory(int position)
        {
            history.Add(new Memento
            {
                GameBoardState = (char[])boardPositions.Clone(),
                PlayerState = player,
                BoardPosition = position
            });
        }
        public static void UndoMove()
        {
            if (history.Count > 0)
            {
                var lastMove = history.Last();

                boardPositions[lastMove.BoardPosition] = (char)(lastMove.BoardPosition + '0');

                if (history.Remove(lastMove))
                {
                    player = (player % 2) + 1;
                }
            }
            else
            {
                Console.WriteLine("No previous moves to undo!");
            }
        }

    }
    public interface IAI
    {
        int MakeMove(char[] board);
    }

    public class SimpleAI : IAI
    {
        public int MakeMove(char[] boardPositions)
        {
            int winMove = FindWinningMove(boardPositions, 'O');
            if (winMove != -1)
                return winMove;

            int preventLossMove = FindWinningMove(boardPositions, 'X');
            if (preventLossMove != -1)
                return preventLossMove;

            Random random = new Random();
            int position;
            do
            {
                position = random.Next(1, 10);
            } while (boardPositions[position] == 'X' || boardPositions[position] == 'O');

            return position;
        }

        private int FindWinningMove(char[] boardPositions, char player)
        {
            for (int i = 1; i < boardPositions.Length; i++)
            {
                if (boardPositions[i] != 'X' && boardPositions[i] != 'O')
                {
                    char[] boardCopy = (char[])boardPositions.Clone();
                    boardCopy[i] = player;
                    if (CheckWin(boardCopy))
                        return i;
                }
            }
            return -1;
        }

        private bool CheckWin(char[] boardPositions)
        {
            // check horizontals
            for (int i = 1; i <= 7; i += 3)
                if (boardPositions[i] == boardPositions[i + 1] && boardPositions[i] == boardPositions[i + 2])
                    return true;

            // check verticals
            for (int i = 1; i <= 3; i++)
                if (boardPositions[i] == boardPositions[i + 3] && boardPositions[i] == boardPositions[i + 6])
                    return true;

            // check diagonals
            if (boardPositions[1] == boardPositions[5] && boardPositions[1] == boardPositions[9])
                return true;
            if (boardPositions[3] == boardPositions[5] && boardPositions[3] == boardPositions[7])
                return true;

            return false;
        }

    }

}