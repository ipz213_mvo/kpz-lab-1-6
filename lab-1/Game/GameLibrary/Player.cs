﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLibrary
{
    public class Player
    {
        private char NamePlayer;
        private int CountOfWin;

        public char Name
        {
            get { return NamePlayer; }
        }
        public int CountWin
        {
            get { return CountOfWin; }
        }
      
        public Player()
        {
            NamePlayer = 'X';
            CountOfWin = 0;
        }
        public Player(char name)
        {
            NamePlayer = name;
            CountOfWin = 0;
        }
        public Player(char name, int countOfWin)
        {
            NamePlayer = name;
            CountOfWin = countOfWin;
        }
        public void AddWin()
        {
            CountOfWin++;
        }
    }
}
