﻿using GameLibrary;
using System.Numerics;

int mode = 0;
int flag = 0;
do
{
    Game.StartGame(mode);
    Console.ForegroundColor = ConsoleColor.Yellow;
    Console.WriteLine($"Do you want to play again? (y/n)");
    Console.ResetColor();
    char continueGame = char.Parse( Console.ReadLine() );
    if(continueGame == 'n' || continueGame == 'N')
    {
        flag= 1;
    }
    else
    {
        mode = 1;
    }
}
while (flag == 0);