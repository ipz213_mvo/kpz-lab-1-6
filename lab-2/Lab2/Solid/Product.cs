﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid
{
    public interface IProduct
    {
        string Title { get; }
        string Description { get; }
        Money Price { get; }
    }
    //Даний клас викорисовує 2 принципи Single Responsibility та Interface Segregation
    //Тому що клас має тільки одну відповідальність це робота з продуктом, а Interface Segregation використовуэться тому що IProduct складається тільки з тих властивостей, які є необхідними для представлення продукту
    public class Product : IProduct
    {
        private string _title;
        private string _description;
        private Money _price;

        public Product(string title, string description, Money price)
        {
            _title = title;
            _description = description;
            _price = price;
        }

        public string Title => _title;

        public string Description => _description;

        public Money Price => _price;

        public void ReducePrice(Money amount)
        {
            _price.Subtract(amount);
        }
        public void UpdateProduct(string title, string description, Money price)
        {
            _title = title;
            _description = description;
            _price = price;
        }
        public override string ToString()
        {
            return $"Title: {_title}\nPrice:{_price.ToString()}\nDescription: {_description}";
        }
    }
}
