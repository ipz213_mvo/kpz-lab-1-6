﻿using System.ComponentModel;

namespace Solid
{
    //Даний інтерфейс надає нам змогу виористати Open/Closed принцип.
    interface IGetCurrency
    {
        string CurrencyName();
    }
    // Клас Money використовує принцип Single responsibility, так як має тільки одну відповідальність це робота з грошима
    public class Money
    {
        private int _wholePart;
        private int _fractionalPart;
        public string currencyName;

        public Money(int wholePart, int fractionalPart)
        {
            _wholePart = wholePart;
            _fractionalPart = fractionalPart;
        }

        public int WholePart 
        {
            get { return _wholePart; }
            set { _wholePart = value; }
        }
        public int FractionalPart
        {
            get { return _fractionalPart; }
            set { _fractionalPart = value; }
        }
        public string CurrencyName
        {
            get { return currencyName; }
        }

        public int GetMoneyInFactionalPart()
        {
            return (_wholePart*100) + _fractionalPart;
        }

        public void Subtract(Money amount)
        {
            if(amount._wholePart > _wholePart || (amount._wholePart == _wholePart && amount._fractionalPart > _fractionalPart))
            {
                Console.WriteLine("Not enough money to subtract.");
            }
            _wholePart -= amount._wholePart;
            _fractionalPart -= amount._fractionalPart;
            if(amount._fractionalPart < 0)
            {
                _wholePart--;
                _fractionalPart = 100;
            }
        }

        public override string ToString()
        {
            return $"{_wholePart}.{_fractionalPart}";
        }
    }
    //В даних класах нащадків використовуються 2 принципи такі як Liskov Substitution та Open/Closed
    //Тому що класи розширють батківський клас а не замінюють його та метод CurrencyName змінюється а не створюється заново
    public class Grivna : Money, IGetCurrency
    {
        public Grivna(int wholePart, int fractionalPart) : base(wholePart, fractionalPart) { }

        public string CurrencyName()
        {
            currencyName = "UAH";
            return currencyName;
        }
    }
    public class Dollar : Money, IGetCurrency
    {
        public Dollar(int wholePart, int fractionalPart) : base(wholePart, fractionalPart) { }

        public string CurrencyName()
        {
            currencyName = "USD";
            return currencyName;
        }
    }
    public class Euro : Money, IGetCurrency
    {
        public Euro(int wholePart, int fractionalPart) : base(wholePart, fractionalPart) { }
        

        public string CurrencyName()
        {

            currencyName = "EUR";
            return currencyName;
        }
    }
}