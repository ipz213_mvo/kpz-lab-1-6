﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid
{
    // Даний інтерфейст позволяє нам використати принцип Interface Segregation для класу WarehouseItem тому що данний інтерфейс складається тільки з тих властивостей, які є необхідними для данного классу
    public interface IWarehouseItem
    {
        Product Product { get; set; }
        DateTime LastArrivalDate { get; set; }
        int CountOfProduct { get; set; }
    }
    // Даний клас виокристовує принцип Single Responsibility
    public class WarehouseItem : IWarehouseItem
    {
        private Product _product;
        private DateTime _lastArrivalDate;
        private int _countOfProduct;
        
        public WarehouseItem(Product product, DateTime lastArrivalDate, int productCount)
        {
            _product = product;
            _lastArrivalDate= lastArrivalDate;
            _countOfProduct= productCount;
        }

        public Product Product { get => _product; set => _product = value; }
        public DateTime LastArrivalDate { get => _lastArrivalDate; set => _lastArrivalDate = value; }
        public int CountOfProduct { get => _countOfProduct; set => _countOfProduct = value; }

        public override string ToString()
        {
            return $"{_product.ToString()}\nCount of Product: {_countOfProduct}\nLast arriaval date: {_lastArrivalDate.ToString()}";
        }
    }
    // Даний клас виокристовує принцип Single Responsibility
    public class Warehouse
    {
        private List<WarehouseItem> _items;
        public Warehouse()
        {
            _items = new List<WarehouseItem>();
        }
        public List<WarehouseItem> Items => _items;
        public void AddItem(WarehouseItem item)
        {
            _items.Add(item);
        }
        public void RemoveItem(WarehouseItem item)
        {
            _items.Remove(item);
        }
    }
}
