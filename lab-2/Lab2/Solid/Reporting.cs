﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid
{
    // Дані інтерфейси надає нам змогу виористати Interface Segregation принцип.
    public interface IInveterisatior
    {
        void Inveteresation(Warehouse warehouse);
    }
    public interface IRegisterIncome
    {
        void RegisterIncome(Warehouse warehouse, Product incomeProduct, DateTime lastArrivalDate, int productCount);
    }
    public interface IGeneratedIncomeRecipt
    {
        void GeneratedIncome(Product incomeProduct, DateTime lastArrivalDate, int productCount);
    }
    public interface IGeneratedOutcomeRecipt
    {
        void GeneratedOutcome(Product outcomeProduct, DateTime lastArrivalDate, int productCount);
    }
    public interface IRegisterOutcome
    {
        void RegisterOutcome(Warehouse warehouse, Product outcomeProduct, DateTime lastArrivalDate, int productCount);
    }
    //Даний клас використовує Single Responsibility та Dependency Inversion, тому що він має тількі одну відповідальність це робота зі звітністю
    //А Dependency Inversion використовується для використання модулів щоб не залежити від модулів вищого рівня
    public class Reporting
    {
        private Warehouse _warehouse;
        public IInveterisatior Inveterisatior { get; set; }
        public IRegisterIncome Income { get; set; }
        public IRegisterOutcome Outcome { get; set; }
        public Reporting(Warehouse warehouse, IInveterisatior inveterisatior, IRegisterIncome income, IRegisterOutcome outcome)
        {
            _warehouse = warehouse;
            Inveterisatior = inveterisatior;
            Income = income;
            Outcome = outcome;
        }
        public void Inveteresation()
        {
            Inveterisatior.Inveteresation(_warehouse);
        }

        public void IncomeRegister(Product product, DateTime dateTime, int count)
        {
            Income.RegisterIncome(_warehouse, product, dateTime, count);
        }

        public void OutcomeRegister(Product product, DateTime dateTime, int count)
        {
            Outcome.RegisterOutcome(_warehouse, product, dateTime, count);
        }
    }
    // Дані класи реалізовують Dependency Inversion принцип та Interface Segregation принцип, так як кожен класс є певним модулем та успадковує інтерфейс тей який потрібен йому
    public class Inveterisator : IInveterisatior
    {
        public void Inveteresation(Warehouse warehouse)
        {
            foreach(var item in warehouse.Items)
            {
                Console.WriteLine($"{item.ToString()}\n");
            }
        }
    }
    public class IncomeRegister : IRegisterIncome, IGeneratedIncomeRecipt
    {
        public void GeneratedIncome(Product incomeProduct, DateTime lastArrivalDate, int productCount)
        {
            Console.WriteLine("Income Recipt");
            Console.WriteLine($" - Product name: {incomeProduct.Title}");
            Console.WriteLine($" - Count of product: {productCount}");
            Console.WriteLine($" - Price per item: {incomeProduct.Price} {incomeProduct.Price.CurrencyName}");
            Console.WriteLine($" - Total price: {productCount * (incomeProduct.Price.GetMoneyInFactionalPart() / 100)} {incomeProduct.Price.CurrencyName}");
            Console.WriteLine($" - Date: {lastArrivalDate.ToString()}");
        }

        public void RegisterIncome(Warehouse warehouse, Product incomeProduct, DateTime lastArrivalDate, int productCount)
        {
            WarehouseItem item = warehouse.Items.Find(p => p.Product.Title == incomeProduct.Title);
            if(item == null)
            {
                item = new WarehouseItem(incomeProduct,lastArrivalDate, productCount);
                warehouse.Items.Add(item);
            }
            else
            {
               item.Product.UpdateProduct(incomeProduct.Title, incomeProduct.Description, incomeProduct.Price);
               item.CountOfProduct = productCount;
               item.LastArrivalDate = lastArrivalDate;
            }
            GeneratedIncome(incomeProduct,lastArrivalDate, productCount);
        }
    }
    public class OutcomeRegister : IRegisterOutcome, IGeneratedOutcomeRecipt
    {
        public void GeneratedOutcome(Product outcomeProduct, DateTime lastArrivalDate, int productCount)
        {
            Console.WriteLine("Outcome Recipt");
            Console.WriteLine($" - Product name: {outcomeProduct.Title}");
            Console.WriteLine($" - Count of product: {productCount}");
            Console.WriteLine($" - Date: {lastArrivalDate}");
        }

        public void RegisterOutcome(Warehouse warehouse, Product outcomeProduct, DateTime lastArrivalDate, int productCount)
        {
            WarehouseItem item = warehouse.Items.Find(p => p.Product.Title == outcomeProduct.Title);
            
            if(item == null)
            {
                Console.WriteLine($"Error: product '{outcomeProduct.Title}' not found");
            }
            else
            {
                item.CountOfProduct -= productCount;
                item.LastArrivalDate = lastArrivalDate;
            }
            GeneratedOutcome(outcomeProduct,lastArrivalDate, productCount);
        }
    }
}
