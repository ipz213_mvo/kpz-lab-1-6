﻿using Solid;

Warehouse warehouse = new Warehouse();

warehouse.AddItem(new WarehouseItem(new Product("Razer mouse", "Mouse for PC", new Grivna(999, 99)), DateTime.Now, 10));
warehouse.AddItem(new WarehouseItem(new Product("IPhone 14", "smartphone", new Dollar(1500, 0)), DateTime.Now, 5));
warehouse.AddItem(new WarehouseItem(new Product("MacBook", "laptop", new Euro(1900, 0)), DateTime.Now, 5));

Reporting report = new Reporting(warehouse, new Inveterisator(), new IncomeRegister(), new OutcomeRegister());
report.Inveteresation();
report.IncomeRegister(new Product("IPhone 14", "smartphone", new Dollar(1500, 0)), DateTime.Now, 10);
report.OutcomeRegister(new Product("MacBook", "laptop", new Euro(1900, 0)), DateTime.Now, 5);